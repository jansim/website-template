# Alumni: `jansim`

![Image of a Mountain at Sunset](https://fastly.picsum.photos/id/794/640/360.jpg?hmac=dtvNek0_1OdHFY4bVi9yuaMiGYB70V6Tvy1bDh8sLD4)

## About me

I've been using git for many years and I'm interested in data science, psychology & open science.

The technologies I typically work with are R, python and the web-stack.
